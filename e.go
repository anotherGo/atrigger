package atrigger

import "errors"

var ErrChanClosed = errors.New("atrigger: the Trigger Channel is Closed")
