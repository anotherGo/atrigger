package atrigger

type Chan chan (interface{})

var Zero = struct{}{}

func (c Chan) Trig(trigger interface{}) error {
	if c.IsClosed() {
		return ErrChanClosed
	} else if trigger == nil {
		c.Close()
	} else {
		c <- trigger
	}
	return nil
}
func (c Chan) Init() Chan {
	c.Close()
	c = make(chan (interface{}))
	return c
}
func (c Chan) Close() {
	if c.IsClosed() {
		return
	}
	close(c)
	c = nil
}
func (c Chan) IsClosed() bool {
	return c == nil
}

func (c Chan) Wait() interface{} {
	if c.IsClosed() == false {
		return <-c
	}
	return nil
}
